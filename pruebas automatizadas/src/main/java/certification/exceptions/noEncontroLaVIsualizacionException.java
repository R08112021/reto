package certification.exceptions;

public class noEncontroLaVIsualizacionException extends AssertionError{
    public noEncontroLaVIsualizacionException(String mensaje,Throwable causa){
        super(mensaje,causa);
    }
}
