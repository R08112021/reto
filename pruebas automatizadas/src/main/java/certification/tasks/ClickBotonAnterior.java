package certification.tasks;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static certification.userinterface.IndexForm.BTN_ANT;

public class ClickBotonAnterior implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BTN_ANT)
        );
    }

    public static ClickBotonAnterior paraVerListado(){
        return Tasks.instrumented(ClickBotonAnterior.class);
    }
}
