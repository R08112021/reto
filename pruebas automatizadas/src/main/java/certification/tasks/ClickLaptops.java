package certification.tasks;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static certification.userinterface.IndexForm.BTN_OPTION_LAPTOP;



public class ClickLaptops implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                WaitUntil.the(BTN_OPTION_LAPTOP, WebElementStateMatchers.isClickable()).forNoMoreThan(60).seconds(),
                Click.on(BTN_OPTION_LAPTOP)
        );
    }
    public static ClickLaptops theCategorias(){
        return Tasks.instrumented(ClickLaptops.class);
    }
}
