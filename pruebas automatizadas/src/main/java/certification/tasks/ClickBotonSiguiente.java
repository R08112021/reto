package certification.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static certification.userinterface.IndexForm.BTN_SIG;
import static certification.userinterface.IndexForm.TITULO_A_RECORDAR;
import static certification.utils.Constantes.TITULO_LAPTOP_ANTES;

public class ClickBotonSiguiente implements Task {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.remember(TITULO_LAPTOP_ANTES,TITULO_A_RECORDAR.resolveFor(actor).getText());
        actor.attemptsTo(
                Click.on(BTN_SIG)
        );
    }
    public static ClickBotonSiguiente recordandoElTitulo(){
        return Tasks.instrumented(ClickBotonSiguiente.class);
    }
}
