package certification.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static certification.userinterface.IndexForm.TITULO_A_RECORDAR;


public class VisualizarCambioDeLaptop implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return TITULO_A_RECORDAR.resolveFor(actor).getText();

    }
    public static VisualizarCambioDeLaptop alDarClickEnBotonSiguiente(){
        return new VisualizarCambioDeLaptop();
    }
}
