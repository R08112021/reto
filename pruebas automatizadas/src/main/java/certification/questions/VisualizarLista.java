package certification.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import static certification.userinterface.IndexForm.CARD_PRIMERA;
import static certification.utils.Constantes.TITULO_PRIMERA_CARTA;

public class VisualizarLista implements Question {
    @Override
    public Object answeredBy(Actor actor) {
       if(CARD_PRIMERA.of(TITULO_PRIMERA_CARTA).resolveFor(actor).isVisible())
       {
           return true;
       }
       else{
           return false;
       }
    }
    public static VisualizarLista deLaSeleccionLaptops(){
        return new VisualizarLista();
    }
}
