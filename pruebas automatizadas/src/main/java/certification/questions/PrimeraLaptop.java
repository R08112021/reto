package certification.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;


import static certification.userinterface.IndexForm.TITULO_A_RECORDAR;



public class PrimeraLaptop implements Question<String> {

    @Override
    public String answeredBy(Actor actor) {
        actor.attemptsTo(
                WaitUntil.the(TITULO_A_RECORDAR, WebElementStateMatchers.isPresent()).forNoMoreThan(60).seconds()
        );
      return TITULO_A_RECORDAR.resolveFor(actor).getText();

            }

    public static PrimeraLaptop despuesDelAnt(){
        return new PrimeraLaptop();
    }

}
