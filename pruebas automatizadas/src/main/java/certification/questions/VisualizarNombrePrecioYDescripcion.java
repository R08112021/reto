package certification.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static certification.userinterface.IndexForm.*;

public class VisualizarNombrePrecioYDescripcion implements Question<Boolean> {
    @Override
    public Boolean answeredBy(Actor actor) {
        int cont=1;
        boolean presencia=true;
        while(CARTA.of(String.valueOf(cont)).resolveFor(actor).isPresent()){
            if(CARTA_TITULO.of(String.valueOf(cont)).resolveFor(actor).getText().equals("")||
                    CARTA_PRECIO.of(String.valueOf(cont)).resolveFor(actor).getText().equals("")||
                    CARTA_DESCRIPCION.of(String.valueOf(cont)).resolveFor(actor).getText().equals("")){
                presencia=false;
                break;
            }
            cont++;
        }
        return presencia;
    }
    public static VisualizarNombrePrecioYDescripcion deLaListaLaptops(){
        return new VisualizarNombrePrecioYDescripcion();
    }
}
