package certification.userinterface;


import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class IndexForm {
    public static final Target BTN_OPTION_LAPTOP=Target.the("click para traer la lista de laptops").located(By.xpath("//a[text()=\"Laptops\"]"));
    public static final Target CARD_PRIMERA=Target.the("carta de primera laptop").locatedBy("//div[@class=\"card h-100\"][1]//a[text()=\"{0}\"]");
    public static final Target TITULO_A_RECORDAR=Target.the("titulo a recordar").located(By.xpath("(//h4[@class=\"card-title\"])[1]//a"));
    public static final Target BTN_ANT=Target.the("boton anterior").located(By.id("prev2"));
    public static final Target BTN_SIG=Target.the("boton siguiente").located(By.id("next2"));
    public static final Target CARTA_TITULO=Target.the("titulo de la carta").locatedBy("(//h4[@class=\"card-title\"])[{0}]//a");
    public static final Target CARTA_PRECIO=Target.the("precio de la carta").locatedBy("(//div[@class=\"card-block\"])[{0}]//h5");
    public static final Target CARTA_DESCRIPCION=Target.the("descripción de la carta").locatedBy("(//p[@class=\"card-text\"])[{0}]");
    public static final Target CARTA=Target.the("carta").locatedBy("(//div[@class=\"card h-100\"])[{0}]");
}
