package certification.stepsdefinitions;

import certification.exceptions.noEncontroLaVIsualizacionException;
import certification.questions.PrimeraLaptop;
import certification.questions.VisualizarCambioDeLaptop;
import certification.questions.VisualizarLista;
import certification.questions.VisualizarNombrePrecioYDescripcion;
import certification.tasks.ClickBotonAnterior;
import certification.tasks.ClickBotonSiguiente;
import certification.tasks.ClickLaptops;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import net.thucydides.core.annotations.Managed;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import static certification.utils.Constantes.*;

public class ListarLaptopsStepDefinitions {
    @Managed
    WebDriver driver;
    @Before
    public void OnStage()
    {
        OnStage.setTheStage(Cast.whereEveryoneCan(BrowseTheWeb.with(driver)));
        OnStage.theActorCalled(ACTOR);
    }

    @Dado("^el usuario se encuetra en la página demoblaze$")
    public void elUsuarioSeEncuetraEnLaPáginaDemoblaze() {
        OnStage.theActorInTheSpotlight().wasAbleTo(Open.url(URL_PAG));
    }

    @Cuando("^el usuario le da click en el boton categoria laptops\\.$")
    public void elUsuarioLeDaClickEnElBotonCategoriaLaptops() {
        OnStage.theActorInTheSpotlight().attemptsTo(ClickLaptops.theCategorias());
    }

    @Entonces("^el usuario verá los dispositivos$")
    public void elUsuarioVeráLosDispositivos() {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(
                VisualizarLista.deLaSeleccionLaptops(), Matchers.is(true)).orComplainWith(noEncontroLaVIsualizacionException.class,"")
        );
    }

    @Dado("^luego le de click al boton siguiente$")
    public void luegoLeDeClickAlBotonSiguiente() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                ClickBotonSiguiente.recordandoElTitulo()
        );
    }

    @Cuando("^el usuario le da click en el boton anterior$")
    public void elUsuarioLeDaClickEnElBotonAnterior() {
        OnStage.theActorInTheSpotlight().attemptsTo(
                ClickBotonAnterior.paraVerListado()
        );
    }

    @Entonces("^el usuario podrá ver la lista previamente visualizadá$")
    public void elUsuarioPodráVerLaListaPreviamenteVisualizadá() {
        String hope=OnStage.theActorInTheSpotlight().recall(TITULO_LAPTOP_ANTES);
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(
                        PrimeraLaptop.despuesDelAnt(),Matchers.is(hope)
                )
        );
    }
    @Entonces("^el usuario podra ver una lista de dispositivos donde en cada dispositivo se visualizara nombre, precio y descripcion$")
    public void elUsuarioPodraVerUnaListaDeDispositivosDondeEnCadaDispositivoSeVisualizaraNombrePrecioYDescripcion() {

        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(
                        VisualizarNombrePrecioYDescripcion.deLaListaLaptops(),Matchers.is(true)
                )
        );
    }
    @Entonces("^Visualizo que las laptops sean diferentes al listado anterior comparando el primer item de la lista$")
    public void visualizoQueLasLaptopsSeanDiferentesAlListadoAnteriorComparandoElPrimerItemDeLaLista() {
        String hope=OnStage.theActorInTheSpotlight().recall(TITULO_LAPTOP_ANTES);
        OnStage.theActorInTheSpotlight().should(
                GivenWhenThen.seeThat(
                        VisualizarCambioDeLaptop.alDarClickEnBotonSiguiente(),Matchers.not(hope)
                )
        );
    }
}
