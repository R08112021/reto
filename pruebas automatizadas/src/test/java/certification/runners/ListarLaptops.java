package certification.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features="src\\test\\resources\\features\\listar_laptops_exitoso.feature",
        glue = "certification\\stepsdefinitions",
        snippets = SnippetType.CAMELCASE


)
public class ListarLaptops {
}
