#language:es

  Característica: Validar los productos laptops en la pagina web
    Yo como candidato qa
    Necesito automatizar la pagina demoblaze
    Para validar la visualización de los productos tipo laptop
    Antecedentes:
      Dado el usuario se encuetra en la página demoblaze
      Cuando el usuario le da click en el boton categoria laptops.

    Escenario: carga exitosa de dispositivos
      Entonces el usuario verá los dispositivos

      Escenario: Carga exitosa despues de click anterior
        Y luego le de click al boton siguiente
        Cuando el usuario le da click en el boton anterior
        Entonces el usuario podrá ver la lista previamente visualizadá

    Escenario: visualizacion exitosa de nombre, precio y descripcion
      Entonces el usuario podra ver una lista de dispositivos donde en cada dispositivo se visualizara nombre, precio y descripcion


      Escenario: Validar boton siguiente
        Cuando luego le de click al boton siguiente
        Entonces Visualizo que las laptops sean diferentes al listado anterior comparando el primer item de la lista